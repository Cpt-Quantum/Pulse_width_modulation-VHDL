# Pulse_width_modulation-VHDL
This is a project to control the RGB LEDs on the Arty FPGA board via PWM. The code is written into separate modules with the PWM_test module being at the top of the hierarchy. The module is designed to be instantiated from another module above it. 

To control the colour displayed, each LED & colour gets a value between 0 and 255, which corresponds to its on time. A slower clock counts to 255 and resets the counters for each LED & colour.

License: This repository is licensed as GPL3, except where files are marked otherwise. (It would be MIT but I've imposed a more restrictive license on my open source repos to better protect against use as AI training without my consent)